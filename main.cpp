#include <iostream>

#include "signature.h"

int main(int argc, char* argv[]) {
  auto params = ParseArguments(argc, argv);
  if (!params.is_success) {
    std::cout << params.error_message << '\n';
    return -1;
  }
  CalculateSignature(params);
  std::cout << "Success\n";
}
